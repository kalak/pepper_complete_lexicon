#!/usr/bin/env python
# coding: utf-8

"""
Version 0.0.1
This program helps to add new words in lexicons for Pepper robot projects.
"""
import argparse


def gromming_file_words(args, lexicon_content):
    """
    * Parameters:
    args with its different variables
    lexicon_content is the content of the destination file
    * Effect:
    This method adds word contained in source file to the list of words in args
    No duplicates and sorted alphabetically.
    * Return:
    None
    """
    if not args.word:
        args.word = list()
    with open(args.source, 'r') as src:
        lines = src.readlines()
    for line in lines:
        if line not in lexicon_content:
            args.word.append(line)


def gromming_list_words(args, lexicon_content):
    """
    * Parameters:
    args with its different variables
    lexicon_content is the content of the destination file
    * Effect:
    This method removes duplicate words in the list of words to add and
    sorts it alphabetically.
    * Return:
    None
    """
    args.word = list(set(args.word))
    args.word.sort()
    for word in args.word:
        if word in lexicon_content:
            args.word = [i for i in args.word if i != word]


def gromming_words_to_add(args):
    """
    * Paramters:
    args with his different variables
    * Effect:
    Meta gromming function to orientate to the good method
    after open the destination file
    * Return
    None
    """
    with open(args.file, 'r') as lexicon:
        lexicon_content = lexicon.read()
    if args.word:
        gromming_list_words(args, lexicon_content)
    else:
        print('You can use too: -w')
    if args.source:
        gromming_file_words(args, lexicon_content)
    else:
        print('You can use too: -s')


def add_words_in_lexicon(args):
    """
    * Parameters:
    args with his variables word and file
    * Effect:
    This method adds the word in the destination file.
    * Return:
    None
    """
    with open(args.file, 'a') as dst:
        try:
            result = ''.join(args.word)
        except:
            print(('Need your attention, the words add in the end of file '
                   'without layout'))
        else:
            dst.write(result)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', type=str,
                        help='Name of file where add conjugate verbs',
                        required=True)
    parser.add_argument('-s', '--source', type=str,
                        help='Name of file with data to import')
    parser.add_argument('-w', '--word', nargs='+',
                        help='Type a serial of words to add')
    args = parser.parse_args()
    gromming_words_to_add(args)
    add_words_in_lexicon(args)
